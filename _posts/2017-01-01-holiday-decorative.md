---
title: holiday decorative
description: Lorem ipsum dolor sit amet adipisicing
img: /assets/img/holiday-decorative.jpg
layout: default
product-types:
 - product-type: Holiday Decorative One
   price: €15.00
 - product-type: Holiday Decorative Two
   price: €35.00
 - product-type: Holiday Decorative Three
   price: €69.00
 - product-type: Holiday Decorative Four
   price: €40.00
---