---
title: GUL
layout: default
---

{% include home-cover.html %}

<div id="products">
{% for post in site.posts %}
	<div class="row product">
		{% assign value = forloop.index | modulo:2 %}
		{% if value != 0 %}
		<div class="col-md-6 col-12 product-cover order-1 order-md-1">
		{% else %}
		<div class="col-md-6 col-12 product-cover order-1 order-md-12">
		{% endif %}
			<img src='{{ post.img | relative_url }}' alt="lamp">
			<div class="product-cover-info">
				<h3>{{ post.title }}</h3>
				<span class="description">{{ post.description }}</span>
				<span><span>Excepteur sint</span> occaecat</span>
				<button class="btn btn-dark btn-sm mt-4">
					shop now
					<i class="fas fa-chevron-right"></i>
				</button>
			</div>
		</div>
		{% if value != 0 %}
		<div class="col-md-6 col-12 product-types order-12 order-md-12">
		{% else %}
		<div class="col-md-6 col-12 product-types order-12 order-md-1">
		{% endif %}
			{% if post.product-types %}
			{% for product-types in post.product-types %}
			<div class="product-type align-items-end flex-column-reverse">
				{% if product-types.in-cart %}
					<div class="corner-cart">
						<i class="fas fa-shopping-cart"></i>
					</div>
				{% endif %}
				<div class="product-type-end justify-content-between align-self-stretch align-items-center">
					<div class="product-type-price-name">
						<span class="product-type-price">{{ product-types.price }}</span>
						<span class="product-type-name">{{ product-types.product-type }}</span>
					</div>
					<span class="product-type-favorite-btn">
						{% if product-types.in-cart %}
						<i class="fas fa-heart gold-color"></i>
						{% else %}
						<i class="far fa-heart"></i>
						{% endif %}
					</span>
				</div>
			</div>
			{% endfor %}
			{% endif %}
		</div>
	</div>
{% endfor %}
</div>

<!--
This is a comment
-->
